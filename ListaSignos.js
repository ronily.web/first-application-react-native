import React from 'react';
import { StyleSheet, View, Button} from 'react-native';
import signos from './signos.json';

export default class App extends React.Component {

  onPress = (idSigno) => {
    this.history.push('/${idSigno}');
  }

  render() {
    /**
     * retornando outra view para testes 
    return (
      <DetalheSigno idSigno={1}/>
    )
    */

    return (
      <View style={styles.container}>
        {signos.map((signo, key) => (
          <Button 
            key={key} 
            title={signo.nome} 
            onPress={() => this.onPress(key)}/>
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
