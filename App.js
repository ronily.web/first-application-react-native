import React from 'react';
import { View, StyleSheet } from 'react-native';
import DetalheSigno from './DetalheSigno';
import ListaSignos from './ListaSignos';
import { NativeRouter as Router, Switch, Route } from 'react-router-native'

export default class App extends React.Component {
  render() {
    return (
      <Router>
        <View style={styles.container}>
          <Switch>
            <Route path='/:idSigno' Component={DetalheSigno}/>
            <Route path='/' Component={ListaSignos}/>
          </Switch>
        </View>
      </Router>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});
