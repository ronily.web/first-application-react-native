import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import signos from './signos.json';

export default class DetalheSigno extends React.Component {

  constructor() {
    super();

    this.state = {
      signo: undefined
    }
  }

  componentDidMount() {
    const idSigno = parseInt(this.props.match.params.idSigno);
    this.setState({
      signo: signos
        .filter((signo, key) => idSigno === key)
        .shift()
    })
  }

  onPress = () => {
    this.history.push('/');
  }

  render() {
    const { signo } = this.state

    if(!signo) {
      return <View />
    }

    return (
      <View style={styles.container}>
        <Text style={style.title}>{signo.nome}</Text>
        <Text style={style.text}>{signo.idade}</Text>
        <Text style={style.skill}>{signo.halidades.join(' , ')}</Text>
        <Button title="Voltar" onPress={ () => this.onPress()}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15
  },
  title: {
    fontSize: 30,
    marginBottom: 10,
    fontWeight: 'bold'
  },
  text: {
    fontSize: 20,
    marginBottom: 10
  },
  skill: {
    fontSize: 20
  }
});
